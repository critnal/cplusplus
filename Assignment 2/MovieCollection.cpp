//MovieCollection.cpp

#pragma once
#include "stdafx.h"
#include "Movie.h"
#include "MovieCollection.h"
#include "MovieCollectionNode.h"





//--- definition for MovieCollection()
MovieCollection::MovieCollection()
{
	root = 0;
}





//--- definition for isEmpty()
bool MovieCollection::isEmpty() const
{
	return (root == 0);
}





//--- definition for destructor()
void MovieCollection::destructor(const MovieCollectionNode * r)
{
	if(r!=0)
	{
		destructor( r->getLChild());
		destructor( r->getRChild());
		delete r;
	}
}





//--- definition for insert()
void MovieCollection::insert(Movie *item)
{
	if(root == 0)
		root = new MovieCollectionNode(item);
	else
		insert(item, root);
}





//--- definition for insert()
void MovieCollection::insert(Movie *item, MovieCollectionNode * ptr)
{
	if (item->getTitle().compare(ptr->item->getTitle()) < 0)
	{
		if (ptr->getLChild() == 0)
			ptr->setLChild(new MovieCollectionNode(item));
		else 
			insert(item, ptr->getLChild());
	}
	else 
	{
		if (ptr->getRChild() == 0)			
			ptr->setRChild(new MovieCollectionNode(item));
		else 
			insert(item, ptr->getRChild());
	}
} 





//--- definition for remove()
void MovieCollection::remove(Movie *item, bool isStaff)
{
	// search for item and its parent
	MovieCollectionNode * ptr = root; // search reference
	MovieCollectionNode * parent = 0; // parent of ptr
	while((ptr!=0)&&(item->getTitle() != ptr->item->getTitle()))
	{
		parent = ptr;
		if(item->getTitle().compare(ptr->item->getTitle()) < 0) // move to the left child of ptr
			ptr = ptr->getLChild();
		else
			ptr = ptr->getRChild();
	}
	if(ptr != 0) // if the search was successful
	{
		// case 3: item has two children
		if((ptr->getLChild() != 0)&&(ptr->getRChild() != 0))
		{
			// find the right-most node in left subtree of ptr
			if(ptr->getLChild()->getRChild() == 0) // a special case
			{
				MovieCollectionNode * temp = ptr->getLChild();
				ptr->item = ptr->getLChild()->item;
				ptr->setLChild(ptr->getLChild()->getLChild());
				if (!isStaff || temp->item->checkNoRents())
					delete temp;
				else
					cout<<"\nMovie cannot be deleted as it is being rented";
			}
			else 
			{
				MovieCollectionNode * p = ptr->getLChild()->getRChild();
				MovieCollectionNode * pp = ptr->getLChild(); // parent of p
				while(p->getRChild() != 0)
				{
					pp = p;
					p = p->getRChild();
				}
				// copy the item at p to ptr
				Movie *temp = p->item;
				ptr->item = temp;
				MovieCollectionNode * temp_pointer = p->getLChild();
				pp->setRChild (temp_pointer);
				if (!isStaff || p->item->checkNoRents())
					delete p;
				else
					cout<<"\nMovie cannot be deleted as it is being rented";
			}
		}
		else // cases 1 & 2: item has no or only one child
		{
			MovieCollectionNode * c;
			if(ptr->getLChild() != 0) c = ptr->getLChild();
			else
				c = ptr->getRChild();
			// remove node ptr
			if(ptr == root) //need to change root
				root = c;
			else
				if(ptr == parent->getLChild()) parent->setLChild (c);
				else
					parent->setRChild (c);
			if (!isStaff || ptr->item->checkNoRents())
				delete ptr;
			else
				cout<<"\nMovie cannot be deleted as it is being rented";
		}
	}
	else
		cout<<"\nMovie does not exist";
}





//--- definition for search()
bool MovieCollection::search(const Movie item) const
{
	return search(item, root);
}





//--- definition for search()
bool MovieCollection::search(const Movie item, MovieCollectionNode * r) const
{
	if(r != 0)
	{
		if(item.getTitle() == r->item->getTitle())
			return true;
		else
			if(item.getTitle().compare(r->item->getTitle()) < 0)
			return search(item, r->getLChild());
		else
			return search(item, r->getRChild());
	}
	else
		return false;
}





//--- definition for preOrderTraverse()
void MovieCollection::preOrderTraverse() const
{
	preOrderTraverse(root);
	cout<< endl;
}





//--- definition for preOrderTraverse()
void MovieCollection::preOrderTraverse(MovieCollectionNode * root) const
{
	if(root != 0)
	{
		cout <<"\n" + root->item->getTitle();	
		preOrderTraverse(root->getLChild());
		preOrderTraverse(root->getRChild());
	}
}





//--- definition for inOrderTraverse()
void MovieCollection::inOrderTraverse() const
{
	cout<< "InOrder: " << endl;
	inOrderTraverse(root);
	cout << endl;
}





//--- definition for inOrderTraverse()
void MovieCollection::inOrderTraverse(MovieCollectionNode * root) const
{
	if(root != 0)
	{
		inOrderTraverse(root->getLChild());
		cout << root->item->getTitle();
		inOrderTraverse(root->getRChild());
	}
}





//--- definition for postOrderTraverse()
void MovieCollection::postOrderTraverse() const
{
	cout << "PostOrder: " << endl;
	postOrderTraverse(root);
	cout << endl;
}





//--- definition for postOrderTraverse()
void MovieCollection::postOrderTraverse(MovieCollectionNode * root) const
{
	if(root != 0)
	{
		postOrderTraverse(root->getLChild());
		postOrderTraverse(root->getRChild());
		cout << root->item->getTitle();
	}
}





//--- definition for movieListTraverse()
void MovieCollection::movieListTraverse(MovieList *browseMovieList) const
{
	cout << "MovieList Traverse: " << endl;;
	movieListTraverse(root, browseMovieList);
	cout<< endl;
}





//--- definition for movieListTraverse()
void MovieCollection::movieListTraverse(MovieCollectionNode * root, MovieList *browseMovieList) const
{
	if(root != 0)
	{
		browseMovieList->insert(root->item);
		movieListTraverse(root->getLChild(), browseMovieList);
		movieListTraverse(root->getRChild(), browseMovieList);
	}
}





//--- definition for dvdAdd()
bool MovieCollection::dvdAdd(Movie *item, const int noDvdAdd)
{
	return dvdAdd(item, noDvdAdd, root);
}





//--- definition for dvdAdd()
bool MovieCollection::dvdAdd(Movie *item, const int noDvdAdd, MovieCollectionNode * r)
{
	if(r != 0)
	{
		if(item->getTitle() == r->item->getTitle())
		{
			r->item->setDvds(noDvdAdd);
			return true;
		}
		else
			if(item->getTitle().compare(r->item->getTitle()) < 0)
			return dvdAdd(item, noDvdAdd, r->getLChild());
		else
			return dvdAdd(item, noDvdAdd, r->getRChild());
	}
	else
		return false;
}





//--- definition for dvdAddToTotal()
bool MovieCollection::dvdAddToTotal(Movie *item, const int noDvdAdd)
{
	return dvdAddToTotal(item, noDvdAdd, root);
}





//--- definition for dvdAddToTotal()
bool MovieCollection::dvdAddToTotal(Movie *item, const int noDvdAdd, MovieCollectionNode * r)
{
	if(r != 0)
	{
		if(item->getTitle() == r->item->getTitle())
		{
			r->item->setDvdsTotal(noDvdAdd);
			return true;
		}
		else
			if(item->getTitle().compare(r->item->getTitle()) < 0)
			return dvdAddToTotal(item, noDvdAdd, r->getLChild());
		else
			return dvdAddToTotal(item, noDvdAdd, r->getRChild());
	}
	else
		return false;
}





//--- definition for movInfo()
void MovieCollection::movInfo(const Movie item) const
{
	movInfo(item, root);
}





//--- definition for movInfo()
void MovieCollection::movInfo(const Movie item, MovieCollectionNode * r) const
{
	if(r != 0)
	{
		if(item.getTitle() == r->item->getTitle())
			r->item->getInfo();
		else
			if(item.getTitle().compare(r->item->getTitle()) < 0)
			movInfo(item, r->getLChild());
		else
			movInfo(item, r->getRChild());
	}
	else
		cout<<"Movie does not exist";
}





//--- definition for dvdSubtract()
bool MovieCollection::dvdSubtract(const Movie *item)
{
	return dvdSubtract(item, root);
}





//--- definition for dvdSubtract()
bool MovieCollection::dvdSubtract(const Movie *item, MovieCollectionNode * r)
{
	if(r != 0)
	{
		if(item->getTitle() == r->item->getTitle())
		{
			return r->item->checkOutDvd();
		}
		else
			if(item->getTitle().compare(r->item->getTitle()) < 0)
			return dvdSubtract(item, r->getLChild());
		else
			return dvdSubtract(item, r->getRChild());
	}
	else
		return false;
}





//--- definition for getMovie()
Movie *MovieCollection::getMovie(string movieName) const
{
	return getMovie(movieName, root);
}





//--- definition for getMovie()
Movie *MovieCollection::getMovie(string movieName, MovieCollectionNode * r) const
{
	if(r != 0)
	{
		if(movieName == r->item->getTitle())
			return r->item;
		else
			if(movieName.compare(r->item->getTitle()) < 0)
			return getMovie(movieName, r->getLChild());
		else
			return getMovie(movieName, r->getRChild());
	}
	return 0;
}





//--- definition for ~MovieCollection()
MovieCollection::~MovieCollection()
{
	destructor(root);
}