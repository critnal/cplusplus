//VideoStoreManagement.cpp

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"
#include "Customer.h"
#include "CustomerCollection.h"
#include "CustomerCollectionNode.h"
#include "Movie.h"
#include "MovieCollection.h"
#include "MovieCollectionNode.h"
#include "MovieList.h"
#include "MovieListNode.h"



//--- Fuction Prototypes
bool login();
void staffMenu();
void customerMenu();

//--- Staff Menu Prototypes
void addMovie();
string chooseMovieGenre();
string chooseMovieClassify();
void addDVD();
void removeMovie();
void addCustomer();
void removeCustomer();
void searchPhoneNo();
void searchRentingMovie();

//--- Customer Menu Prototypes
void browseMovies();
void showMovieInfo();
void rentDVD();
void returnDVD();
void showCurrentlyRenting();
void showTopTen();



//--- Initial Objects
MovieCollection storeMovieCollection;
Movie *movie1 = new Movie ("Sherlock Holmes", 128, "Robert Downey Jr.; Jude Law", "Guy Ritchie", "Action", "M15+", 2009, 1, 1);
Movie *movie2 = new Movie ("The Shawshank Redemption", 142, "Tim Robbins; Morgan Freeman; Bob Gunton", "Frank Darabont", "Drama", "M15+", 1997, 2, 2);
Movie *movie3 = new Movie ("The Godfather", 175, "Marlon Brando; Al Pacino; James Caan", "Francis Ford Coppola", "Drama", "MA15+", 1972, 2, 2);
Movie *movie5 = new Movie ("Pulp Fiction", 154, "John Travolta; Uma Thurman; Samuel L. Jackson", "Quentin Tarantino", "Thriller", "MA15+", 1994, 2, 2);
Movie *movie7 = new Movie ("12 Angry Men", 96, "Henry Fonda; Lee J. Cobb; Martin Balsam", "Sidney Lumet", "Drama", "PG", 1957, 2, 2);
Movie *movie4 = new Movie ("Schindler's List", 195, "Liam Neeson; Ralph Fiennes; Ben Kingsley", "Steven Spielberg", "Drama", "M15+", 1993, 2, 2);
Movie *movie6 = new Movie ("The Matrix", 136, "Keanu Reeves; Laurence Fishburne; Carrie-Anne Moss", "Andy Wachowski; Lana Wachowski", "SciFi", "M15+", 1999, 2, 2);

CustomerCollection storeCustomerCollection;
Customer * customer1 = new Customer ("Holt", "Caleb", "1809 Southern Avenue", "0431864635", 1655);
Customer * customer2 = new Customer ("Ratcliff", "Joanne", "4387 North Street", "0434929252", 6874);
Customer * customer3 = new Customer ("Pearse", "Zachary", "4391 Marie Street", "0408523457", 28634);
Customer * customer4 = new Customer ("Bailey", "Ashton", "1712 Stoney Lane", "0495273984", 964185);
Customer * customer5 = new Customer ("Booth", "Nicholas", "4607 Canterbury Drive", "0403646619", 030405);
Customer * customer6 = new Customer ("McConachy", "Koby", "43 Poco Mas Drive", "0445621477", 556612);
Customer * customer7 = new Customer ("Toll", "Caitlyn", "4 Sunny Day Drive", "0471487362", 664578);

bool quitAll = false;
string currentCustomer;





//pre:  true
//post: runs program
int main()
{
	// Insert initial objects
	storeMovieCollection.insert(movie1);
	storeMovieCollection.insert(movie2);
	storeMovieCollection.insert(movie3);
	storeMovieCollection.insert(movie4);
	storeMovieCollection.insert(movie7);
	storeMovieCollection.insert(movie5);
	storeMovieCollection.insert(movie6);
	
	storeCustomerCollection.insert(customer1);
	storeCustomerCollection.insert(customer5);
	storeCustomerCollection.insert(customer4);
	storeCustomerCollection.insert(customer2);
	storeCustomerCollection.insert(customer3);
	storeCustomerCollection.insert(customer7);
	storeCustomerCollection.insert(customer6);

	while (!quitAll)
	{
		bool staffMenuCheck = login();
		if (staffMenuCheck)
			staffMenu();
		else		
			customerMenu();
		
		cout<<"\n\nExit entire program? (y/n): ";
		string choice;
		bool validChoice = false;
		while (!validChoice)
		{
			getline(cin, choice);
			if ((choice == "y") || (choice == "Y"))
			{
				quitAll = true;
				validChoice = true;
			}
			else if ((choice == "n") || (choice == "N"))
			{
				validChoice = true;
			}
			else
			{
				cout<<"\nERROR: Invalid entry, please enter y for yes or n for no: ";
			}
		}
	}
	return 0;
}





//pre:  true
//post: verify login details.
// return staff if staff login details
bool login()
{
	bool staff = false;
	bool validLogin = false;
	string userName;
	while (!validLogin)
	{
		cout<<"\n\nEnter username: ";
		getline(cin, userName);
		int correctPword = storeCustomerCollection.getLoginInfo(userName);
		if (userName == "staff")
		{
			string password;
			cout<<"\nEnter password: ";
			getline(cin, password);
			if (password == "today123")
			{
				cout<<"\n\n Login successful";
				validLogin = true;
				staff = true;
			}
			else 
			{
				cout<<"\nERROR: Invalid password";
			}
		}
		else if (correctPword > 0)
		{
			stringstream stream;
			string tempLine;
			int password;
			cout<<"\nEnter password: ";
			getline(cin, tempLine);
			stream << tempLine;
			stream >> password;
			if (password == correctPword)
			{
				cout<<"\n\nLogin Successful";
				validLogin = true;
				staff = false;
				currentCustomer = userName;
			}
			else
				cout << "\nERROR: Invalid password";
		}
		else
		{
			cout<<"\nERROR: Invalid username";
		}
	}
	return staff;
}





//pre:  true
//post: displays staff main menu
void staffMenu()
{
	bool stopMenu = false;
	int choice;

	const int QUIT_MENU = 0;
	const int MAX_CHOICE = 7;
	const int NEW_MOV =  1;
	const int NEW_DVD = 2;
	const int REMOVE_DVD = 3;
	const int NEW_CUSTOMER = 4;
	const int REMOVE_CUSTOMER = 5;
	const int SEARCH_PHONE = 6;
	const int SEARCH_MOV = 7;

	while (!stopMenu)
	{
		stringstream stream;
		string tempLine;

		cout << endl << endl;
		cout<<"\n\nVideo Store Staff Menu";
		cout<<"\n\n\t1. Add a New Movie";
		cout<<"\n\t2. Add DVDs of Existing Movie";
		cout<<"\n\t3. Remove a DVD";
		cout<<"\n\t4. Register New Customer";
		cout<<"\n\t5. Remove Registered Customer";
		cout<<"\n\t6. Search Customer Phone Number";
		cout<<"\n\t7. Search Customer Rentals";
		cout<<"\n\t0. Exit System"<<endl;
		getline(cin, tempLine);
		stream << tempLine;
		stream >> choice;
		if ((choice >= 0) && (choice <= MAX_CHOICE))
		{
			switch (choice)
			{
			case QUIT_MENU:
				stopMenu = true;
				break;

			case NEW_MOV:
				addMovie();
				break;

			case NEW_DVD:
				addDVD();
				break;

			case REMOVE_DVD:
				removeMovie();
				break;

			case NEW_CUSTOMER:
				addCustomer();
				break;

			case REMOVE_CUSTOMER:
				removeCustomer();
				break;

			case SEARCH_PHONE:
				searchPhoneNo();
				break;

			case SEARCH_MOV:
				searchRentingMovie();
				break;
			}
		}
		else
		{
			cout<<"\n\nERROR: Invalid selection, please enter a valid number";
		}
	} 
}





//pre:  true
//post: displays customer main menu
void customerMenu()
{
	bool stopMenu = false;
	int choice;

	const int QUIT_MENU = 0;
	const int MAX_CHOICE = 6;
	const int BROWSE_MOV = 1;
	const int MOV_INFO = 2;
	const int RENT_DVD = 3;
	const int RETURN_DVD = 4;
	const int VIEW_RENTED = 5;
	const int TOP_TEN = 6;

	while (!stopMenu){
		stringstream stream;
		string tempLine;

		cout<<"\n\nVideo Store Customer Menu";
		cout<<"\n\n\t1. Browse Movies";
		cout<<"\n\t2. Lookup Movie Info";
		cout<<"\n\t3. Rent a DVD";
		cout<<"\n\t4. Return a DVD";
		cout<<"\n\t5. View My Rentals";
		cout<<"\n\t6. View My Top Ten";
		cout<<"\n\t0. Exit System"<<endl;

		getline(cin, tempLine);
		stream << tempLine;
		stream >> choice;

		if ((choice >= 0) && (choice <= MAX_CHOICE))
		{
			switch (choice)
			{
			case QUIT_MENU:
				stopMenu = true;
				break;

			case BROWSE_MOV:
				browseMovies();
				break;

			case MOV_INFO:
				showMovieInfo();
				break;

			case RENT_DVD:
				rentDVD();
				break;

			case RETURN_DVD:	
				returnDVD();
				break;

			case VIEW_RENTED:
				showCurrentlyRenting();
				break;

			case TOP_TEN:
				showTopTen();
				break;
			}
		}
		else
		{
			cout<<"\n\nERROR: Invalid selection, please enter a valid number";
		}
	} 
}





//pre:  true
//post: adds a movie to storeCustomerCollection.
// number of DVDs must be specified
void addMovie()
{
	string tempTitle;
	int tempDuration;
	string tempStar;
	string tempDirector;
	string tempGenre;
	string tempClassify;
	int tempRelease;
	string tempLine;
	stringstream stream;
	int tempDvds;

	cout<<"\n\nEnter the title of the movie: ";
	getline(cin, tempTitle);

	cout<<"\nEnter the duration of the movie: ";
	getline(cin, tempLine);
	stream << tempLine;
	stream >> tempDuration;
	stream.str("");
	stream.clear();

	cout<<"\nEnter the Star of the movie: ";
	getline(cin, tempStar);

	cout<<"\nEnter the Director of the movie: ";
	getline(cin, tempDirector);

	cout<<"\nSelect a Genre";
	tempGenre = chooseMovieGenre();

	cout<<"\nSelect a Classification";
	tempClassify = chooseMovieClassify();

	cout<<"\nEnter the year of release: ";
	getline(cin, tempLine);
	stream << tempLine;
	stream >> tempRelease;
	stream.str("");
	stream.clear();

	cout<<"\nEnter the number of DVDs to add: ";
	getline(cin, tempLine);
	stream << tempLine;
	stream >> tempDvds;
	stream.str("");
	stream.clear();

	// make sure user entered an int for tempDvds
	if (tempDvds >= 0)
	{
		Movie * newMov_ptr = new Movie (tempTitle, tempDuration, tempStar, tempDirector, 
										tempGenre, tempClassify, tempRelease, tempDvds, tempDvds);
		storeMovieCollection.insert(newMov_ptr);

		cout << endl << endl << tempTitle << " added";
	}
	else
	{
		cout << "\n\nERROR: number of DVDs must be an integer";
	}
}





//pre:  true
//post: user selects classification of movie being added.
// function called by addMovie()
string chooseMovieClassify()
{
	int choice = -1;
	while ((choice < 1) || (choice > 4))
	{
		stringstream stream;
		string tempLine;

		cout<<"\n\n\t1. G";
		cout<<"\n\t2. PG";
		cout<<"\n\t3. M15+";
		cout<<"\n\t4. MA15+\n";

		getline(cin, tempLine);
		stream << tempLine;
		stream >> choice;

		if ((choice < 1) || (choice > 4))
		{
			cout<<"\n\nERROR: Invalid choice, please re enter";
		}
	}
	switch (choice)
	{
		case 1: return "G";
			break;

		case 2: return "PG";
			break;

		case 3: return "M15+";
			break;

		case 4: return "MA15+";
			break;
	}
	return "";
}





//pre:  true
//post: user selects genre of movie being added.
// function called by addMovie()
string chooseMovieGenre()
{
	int choice = -1;
	while ((choice < 1) || (choice > 9))
	{
		stringstream stream;
		string tempLine;

		cout<<"\n\n\t1. Drama";
		cout<<"\n\t2. Adventure";
		cout<<"\n\t3. Family";
		cout<<"\n\t4. Action";
		cout<<"\n\t5. SciFi";
		cout<<"\n\t6. Comedy";
		cout<<"\n\t7. Animated";
		cout<<"\n\t8. Thriller";
		cout<<"\n\t9. Other\n";

		getline(cin, tempLine);
		stream << tempLine;
		stream >> choice;

		if ((choice < 1) || (choice > 9))
		{
			cout<<"\n\nERROR: Invalid choice, please re enter";
		}
	}
	switch (choice)
	{
	case 1: return "Drama";
		break;

	case 2: return "Adventure";
		break;

	case 3: return "Family";
		break;

	case 4: return "Action";
		break;

	case 5: return "SciFi";
		break;

	case 6: return "Comedy";
		break;

	case 7: return "Animated";
		break;

	case 8: return "Thriller";
		break;

	case 9: return "Other";
		break;
	}
	return "";
}





//pre:  true
//post: adds a specified number of DVDs to a specified movie.
// number must be an int
void addDVD()
{
	string movieName;
	string tempLine;
	stringstream stream;
	int addDvdNo;

	cout<<"\n\nEnter name of the movie you wish to add DVDs to";
	getline(cin, movieName);

	cout<<"\nEnter the number of DVDs you wish to add";
	getline(cin, tempLine);
	stream << tempLine;
	stream >> addDvdNo;

	Movie *tempMovie = storeMovieCollection.getMovie(movieName);

	if (storeMovieCollection.dvdAddToTotal(tempMovie, addDvdNo))
		cout<<endl<<endl<<addDvdNo<<" "<<"DVDs successfully added to "<<movieName;
	else
		cout<<"\nERROR: No movie with that title exists";
}





//pre:  true
//post: removes a movie from storeMovieCollection
void removeMovie()
{
	string movieName;

	cout<<"\n\nEnter the name of the movie you wish to remove\n";
	getline(cin, movieName);
	Movie *removeMovie = storeMovieCollection.getMovie(movieName);

	storeMovieCollection.remove(removeMovie, true);
}





//pre:  true
//post: add a customer to storeCustomerCollection
void addCustomer()
{
	string newLastName,
	       newFirstName,
	       newAddress,
	       newPhoneNo;

	int newPassword;
	bool validPword = false;
	const int MIN_PASSWORD_SIZE = 4;
	const int MAX_PASSWORD_SIZE = 6;
	
	stringstream tempStream;
	string tempLine;


	cout << "Enter customer's last name: ";
	getline(cin, newLastName);

	cout << "Enter customer's first name: ";
	getline(cin, newFirstName);

	cout << "Enter customer's address: ";
	getline(cin, newAddress);

	cout << "Enter customer's phone number: ";
	getline(cin, tempLine);
	tempStream << tempLine;
	tempStream >> newPhoneNo;

	cout << "Enter customer's password: ";
	while (!validPword)
	{
		getline(cin, tempLine);
		if ((tempLine.size() >= MIN_PASSWORD_SIZE) && (tempLine.size() <= MAX_PASSWORD_SIZE))
		{
			tempStream.clear();
			tempStream << tempLine;
			tempStream >> newPassword;
			tempStream.str("");
			tempStream.clear();
			validPword = true;
		}
		else
		{
			cout<<"Invalid password length, please enter a password with between 4-6 digits: ";
		}
	}
	Customer *newCust_Ptr = new Customer(newLastName, newFirstName, newAddress,
										 newPhoneNo, newPassword);
	storeCustomerCollection.insert(newCust_Ptr);
	cout << "new customer";

}





//pre:  true
//post: remove a customer from storeCustomerCollection
void removeCustomer()
{
	string removeLastName,
		   removeFirstName;


	cout << "\n\nEnter customer's last name: ";
	getline(cin, removeLastName);

	cout << "\nEnter customer's first name: ";
	getline(cin, removeFirstName);

	CustomerCollectionNode *removeCustomerNode 
		= storeCustomerCollection.getCustomerNode(removeLastName, removeFirstName);
	storeCustomerCollection.erase(removeCustomerNode);

	cout << endl << "\n\nCustomer " 
		 << removeFirstName << " " << removeLastName 
		 << " deleted";
}





//pre:  true
//post: displays the phone number of a specified customer
void searchPhoneNo()
{
	string tempLastName,
		   tempFirstName;

	string tempPhoneNo;


	cout << "\n\nPlease enter the customer's last name: ";
	getline(cin, tempLastName);

	cout << "\nPlease enter the customer's first name: ";
	getline(cin, tempFirstName);

	tempPhoneNo = storeCustomerCollection.getPhoneNo(tempLastName, tempFirstName);

	cout << "That customer's phone number is " << tempPhoneNo;
}





//pre:  true
//post: displays all customers currently renting the specified movie
void searchRentingMovie()
{
	string movieName;

	cout<<"\n\nEnter the name of the movie you want to lookup rentals of\n";
	getline(cin, movieName);

	Movie *tempMovie = storeMovieCollection.getMovie(movieName);
	storeCustomerCollection.searchRenting(tempMovie);
}





//pre:  true
//post: displays all movies in storeMovieCollection
// in alphabetical order
void browseMovies()
{
	MovieList *browseMovieList = new MovieList();

	storeMovieCollection.movieListTraverse(browseMovieList);
	browseMovieList->displayMovies();
}





//pre:  true
//post: displays all properties of the speficied movie
void showMovieInfo()
{
	string movieName;

	cout<<"\n\nEnter the name of the movie you wish to view information for\n";
	getline(cin, movieName);

	Movie tempMov(movieName, 0, "", "", "", "", 0, 0, 0);
	storeMovieCollection.movInfo(tempMov);
}





//pre:  true
//post: adds a movie to the current customer's MovieCollection
// removes a single DVD of same movie from storeMovieCollection
void rentDVD()
{
	string movieName;
	cout<<"\n\nEnter the name of the movie you wish to rent\n";
	getline(cin, movieName);
	Movie *rentMovie = storeMovieCollection.getMovie(movieName);
	if (!(storeCustomerCollection.checkRenting(currentCustomer, rentMovie)))
	{
		if (storeMovieCollection.dvdSubtract(rentMovie))
		{
			storeCustomerCollection.rentDVD(currentCustomer, rentMovie);
			cout<<"\n\nMovie Rented";
		}
		else
			cout<<"\n\nERROR: Movie does not exist or has no available copies";
	}
	else
		cout<<"\n\nERROR: You are already renting this movie";
}





//pre:  true
//post: removes a movie from current customer's MovieCollection
// adds a single DVD of same movie to storeMovieCollection
void returnDVD()
{
	string movieName;

	cout<<"\n\nEnter the name of the movie you wish to return\n";
	getline(cin, movieName);
	
	if (Movie *returnMovie = storeMovieCollection.getMovie(movieName))
	{
		if (storeCustomerCollection.checkRenting(currentCustomer, returnMovie))
		{

			storeMovieCollection.dvdAdd(returnMovie, 1);
			storeCustomerCollection.returnDVD(currentCustomer, returnMovie);

			cout<<"\n\nMovie Returned";
		}
		else
			cout << "\n\nERROR: You are not currently renting that movie";
	}
	else
		cout << "\n\nERROR: That movie does not exist";
}





//pre:  true
//post: displays all movies currently being rented
void showCurrentlyRenting()
{
	cout<<"\n\nYour current rentals"<<endl;
	storeCustomerCollection.showRentals(currentCustomer);
}





//pre:  true
//post: displays the top 10 most rented movies
void showTopTen()
{
	storeCustomerCollection.showTopTen(currentCustomer);
}