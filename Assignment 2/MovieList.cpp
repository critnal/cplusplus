//CustomerCollection.cpp

#pragma once
#include "stdafx.h"
#include "Movie.h"
#include "MovieList.h"
#include "MovieListNode.h"





//--- definition for MovieList()
MovieList::MovieList()
{ 
   first = 0; 
   mySize = 0;
}





//--- definition for insert()
void MovieList::insert(Movie *newMovie)
{
	// special case
	// if new customer needs to be inserted at the front of the list
	// because it is empty
	if (mySize == 0)
	{ 
		MovieListNode *newMovieNode = new MovieListNode();

		first = newMovieNode;             // first now points to the new node

		newMovieNode->data = newMovie; // the new node's data points to the new customer
		newMovieNode->next = 0;           // the new node's next points to zero
	}

	// special case
	// if new customer needs to be inserted at the front of the list
	// but the list is not empty
	else if (newMovie->getTitle().compare(first->data->getTitle()) < 0)
	{ 
		MovieListNode *newMovieNode = new MovieListNode();

		newMovieNode->data = newMovie; // the new node's data points to the new customer
		newMovieNode->next = first;		 // the new node's next points to the original node

		first = newMovieNode;             // first now points to the new node		
	}

	// general case
	else
	{
		MovieListNode *currNode = first; // *currNode points to first node in list
		MovieListNode *prevNode = 0;     // *prevNode points to zero

		// iterate through the entire list to the correct position
		for (int i = 0; i < mySize; i++)
		{  
			prevNode = currNode;			// the previous node's pointer becomes the current node's pointer

			if (currNode->next != 0)		// if the current node is not the last one
				currNode = currNode->next;  // the current node's pointer points to the next node		

			if (newMovie->getTitle().compare(currNode->data->getTitle()) < 0) // check whether to insert new customer at the current position
				break;	
		}
		MovieListNode *newMovieNode = new MovieListNode();

		prevNode->next = newMovieNode;     // the previous node's next points to the new node
			
		newMovieNode->data = newMovie;  // the new node's data points to the new customer
		newMovieNode->next = currNode;     // the new node's next points to the current node
	}
	mySize++;
}





//--- definition for erase()
void MovieList::erase(MovieListNode *removeMovieNode)
{
	// if the list is empty, display message
	if (mySize == 0)
	{
		cout << "No movies in list";
		return;
	}

	else
	{
		MovieListNode *currNode = first->next; // currNode points to the first node in the list
		MovieListNode *prevNode = first;		// prevNode 

		// iterate through the list until the specified node is found
		for (int i = 0; i < mySize; i++)
		{
			if (currNode = removeMovieNode)
			{
				prevNode->next = currNode->next; // the previous node's next now points to the next node
				delete removeMovieNode;		 // destroy the current node
				mySize--;
				return;
			}
			prevNode = currNode;
			currNode = currNode->next;
		}
		// if the specified node isn't found, display message
		cout << endl << "no match";
		return;
	}
}





//--- definition for getMovieNode()
MovieListNode *MovieList::getMovieNode(string title)
{
	MovieListNode *movieNode = first;

	// iterate through the list until a customer is found
	// with the specified first and last names
	for (int i = 0; i < mySize; i++)
	{
		if ((movieNode->data->getTitle() == title)
			 &&
			(movieNode->data->getTitle() == title))
		{
			return movieNode;
		}
		movieNode = movieNode->next;
	}
	// if the specified customer isn't found, display message
	cout << "No match";
	return movieNode;
}





//--- definition for empty()
bool MovieList::empty() const
{ 
   return (first == 0); 
}





//--- definition for getMySize()
int MovieList::getMySize()
{
	 return mySize;
}





//--- definition for displaymovies()
void MovieList::displayMovies()
{
	MovieListNode *currNode = first;

	for (int i = 0; i < mySize; i++)
	{
		cout << currNode->data->getTitle();

		currNode = currNode->next;
	}
}





//--- definition for ~MovieList()
MovieList::~MovieList()
{     
   // Set pointers to run through the CustomerCollection
   MovieListNode *currPtr = first,  // node to be deallocated
						  *nextPtr;          // its successor
   while (currPtr != 0)
   {
	   nextPtr = currPtr->next;
       delete currPtr;
       currPtr = nextPtr;
   }
   cout <<"destructor \n";
}