//MovieCollection.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"
#include "Movie.h"
#include "MovieList.h"
#include "MovieCollectionNode.h"



// the MovieCollection class represents a single collection of movies
// arranged in a binary search tree.
// the binary tree can be traversed in pre-order, in-order and post-order
class MovieCollection
{
	public:
		//pre:  true
		//post: create an object of MovieCollection
		MovieCollection();
	
		//pre:  true
		//post: return true if the binary tree is empty; otherwise, return false.
		bool isEmpty() const;

		//pre:  true
		//post: item is added to the binary search tree
		void insert(Movie *item);

		//pre:  true
		//post: an occurrence of item is removed from the binary search tree 
		// if item is in the binary search tree
		void remove(Movie *item, bool isStaff);

		//pre:  true
		//post: return true if item is in the binary search true;
		// otherwise, return false.
		bool search(const Movie item) const;

		//pre:  true
		//post: all the nodes in the binary tree are visited once and
		// only once in pre-order
		void preOrderTraverse() const;

		//pre:  true
		//post: all the nodes in the binary tree are visited once and 
		// only once in in-order
		void inOrderTraverse() const;

		//pre:  true
		//post: all the nodes in the binary tree are visited once and only once 
		// in post-order
		void postOrderTraverse() const;

		//pre:  true
		//post: all the nodes in the binary tree are removed and the binary 
		// tree becomes empty
		void clear();		

		//pre:  true
		//post: special pre-order traverse function used to
		// fill a MovieList
		void movieListTraverse(MovieList *browseMovieList) const;

		//pre:  true
		//post: return true if item is in the binary search true;
		// otherwise, return false, adds specified number of Dvds to movie.
		bool dvdAdd(Movie *item, const int noDvdAdd);

		//pre:  true
		//post: return true if item is in the binary search true;
		// otherwise, return false, adds specified number of Dvds to movie.
		bool dvdAddToTotal(Movie *item, const int noDvdAdd);

		//pre:  true
		//post: locates the specified movie in the binary tree
		// and calls that movie's getInfo() function
		void movInfo(const Movie item) const; 
				
		//pre:  true
		//post: remove a single dvd from item
		bool dvdSubtract(const Movie *item);

		//pre:  true
		//post: returns pointer to the movie with title movieName
		Movie *getMovie(string movieName) const;

		//pre:  true
		//post: destroy the object and reclaim the memory used by the object
		~MovieCollection();



	private:
		MovieCollectionNode * root; //the root of the MovieCollection
		
		bool search(const Movie item, MovieCollectionNode * r) const; 
		
		void insert (Movie *item, MovieCollectionNode * ptr);
		
		void preOrderTraverse(MovieCollectionNode * root) const;
		
		void inOrderTraverse(MovieCollectionNode * root) const;
		
		void postOrderTraverse(MovieCollectionNode * root) const;
		
		void destructor(const MovieCollectionNode * r);
		
		void movieListTraverse(MovieCollectionNode * root, MovieList *browseMovieList) const;

		bool dvdAdd(Movie *item, const int noDvdAdd, MovieCollectionNode * r);

		bool dvdAddToTotal(Movie *item, const int noDvdAdd, MovieCollectionNode * r);

		void movInfo(const Movie item, MovieCollectionNode * r) const; 

		bool dvdSubtract(const Movie *item, MovieCollectionNode * r);

		Movie *getMovie(string movieName, MovieCollectionNode * r) const;

}; //--- end of MovieCollection class