//MovieCollectionNode.cpp

#pragma once
#include "stdafx.h"
#include "Movie.h"
#include "MovieCollectionNode.h"





//--- definition for MovieCollectionNode()
MovieCollectionNode::MovieCollectionNode(Movie *newItem)
{
	item = newItem;
	lchild = 0;
	rchild = 0;
}





//--- definition for getLChild()
MovieCollectionNode* MovieCollectionNode::getLChild() const
{
	MovieCollectionNode * p = lchild;
	return p;
}





//--- definition for setLChild()
void MovieCollectionNode::setLChild(MovieCollectionNode * p)
{
	lchild = p;
}





//--- definition for getRChild()
MovieCollectionNode* MovieCollectionNode::getRChild() const
{
	MovieCollectionNode * p = rchild;
	return p;
}





//--- definition for setRChild()
void MovieCollectionNode::setRChild(MovieCollectionNode * p)
{
	rchild = p;
}





//--- definition for ~MovieCollectionNode()
MovieCollectionNode::~MovieCollectionNode()
{
}
