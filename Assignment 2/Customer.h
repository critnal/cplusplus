//Customer.h

//Authors:
//		Haydn Brindley n8318824
//		Alexander Crichton n6878296
//Date:
//		27/05/2012
//Unit:
//		INB371

#pragma once
#include "stdafx.h"
#include "MovieCollection.h"



// the Customer class represents a single customer.
// each customer:
//				-has details such as name, password, etc
//				-can rent and return movies
//				-has access to functions that display their current rentals, etc
class Customer
{
	public:
		//pre:  true
		//post: an object of Customer is created
		// default constructor
		Customer();

		//pre:  true
		//post: an object of Customer is created
		Customer(const string lastName, const string firstName, const string address, const string phoneNo, const int password);

		//pre:  true
		//post: returns customer's last name
		string getMyLName();

		//pre:  true
		//post: returns customer's first name
		string getMyFName();

		//pre:  true
		//post: returns customer's phone number
		string getMyPhoneNo();

		//pre:  true
		//post: displays customers first and last names
		void displayName();

		//pre:  true
		//post: returns customer's password
		int getMyPword();

		//pre:  true
		//post: add item to myMovies
		void addMovie(Movie *item);

		//pre:  true
		//post: remove item from myMovies
		void removeMovie(Movie *item);

		//pre:  true
		//post: returns true if item is in myMovies
		bool checkRenting(Movie *item);

		//pre:  true
		//post: displays movies currently in myMovies
		void showMyRentals();

		//pre:  true
		//post: outputs contents of myTopTen to screen
		void showTopTen();

		//pre:  true
		//post: Adds movies to top ten, or increases the rent counts, and reorders the list
		void doTopTen(string movieName);

		//pre:  true
		//post: object is destroyed and its allocated memory reclaimed
		~Customer();



	private:
		string myLName;
		
		string myFName;
		
		string myAddress;
		
		string myPhoneNo;
		
		string myTopMoviesTitles [10];

		int myTopMoviesRents [10];

		int myPword;
		
		MovieCollection myMovies; // list of movies being rented by
								  // this customer

}; //--- end of Customer class